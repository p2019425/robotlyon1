#include "graph.hpp"
using namespace std;
#include <iostream>


Graph::Graph( Board g) {
	for(pair<Location, Board::TileType> tile : g.tiles) { // on parcours toutes les cases du board 
		for(int i=0; i<4; i++){ // Orientation possible
            sommet s;
			for(int j=0; j<7; j++){ // 7 coups possibles
                Robot r = {tile.first, (Robot::Status) i} ; // création robot
                g.play(r , (Robot::Move) j); // avancée du robot
				Voisins v; // création du voisin
				v.orientation = i;
				v.deplacement = j;
				
                v.robot = r;
                if(v.robot.status!=Robot::Status::DEAD){ //je ne stock plus les robot qui mamen a la mort
                    s.voisins.push_back(v);
                }
			}
            Robot r = {tile.first, (Robot::Status) i};
            graphe[r] = s;
		}
	}
}
}

void Graph::afficheGraph(Board g) 
{	
    for(pair<Location, Board::TileType> cases : g.tiles) { // on parcours toutes les cases du board
        for(int i=0; i<4; i++){ // Orientation possible
            Robot r = {cases.first, (Robot::Status) i} ;
            
            for (size_t i=0; i<graphe[r].voisins.size(); i++){ // v est un voisins
            std::cout << " Je vais de [" << cases.first.column << ","; // rbd
            std::cout << cases.first.line;
            std::cout << ","  <<graphe[r].voisins[i].orientation<<"]";

            std::cout << " avec le coup " <<graphe[r].voisins[i].deplacement<< " ";
            std::cout << " vers [" << graphe[r].voisins[i].robot.location.column << ","; // rbd
            std::cout << graphe[r].voisins[i].robot.location.line<< ",";
            std::cout << static_cast<int>(graphe[r].voisins[i].robot.status)<< "]"<<std::endl;

            // je vais de 1,3,0 à 1,5,0 avec le coup 1 //
	
        }
    }

}
}

